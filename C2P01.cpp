/**********************************************
* File: C2P01.cpp
* Author: Chris Gotuaco, Wonseok Lee, Max Nguyen, Matthew Shan
* Email: cgotuaco@nd.edu, wlee11@nd.edu, mnguye18@nd.edu, mshan@nd.edu
**********************************************/

#include "Supp.h"
/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
* 
* This is the main driver function, taking in a stream from ifstream.  
********************************************/
int main(int argc, char **argv) {
    std::ifstream inputStream;
    getFileStream(inputStream, argv[1]);
    return 0;
}
