/**********************************************
* File: DLLNode.h
* Author: Chris Gotuaco, Wonseok Lee, Max Nguyen, Matthew Shan
* Email: cgotuaco@nd.edu, wlee11@nd.edu, mnguye18@nd.edu, mshan@nd.edu
*  
**********************************************/
#ifndef DLLNODE_H
#define DLLNODE_H
template<class T>
struct Node {
    T data;
    Node *next;
    Node *prev;
};
#endif
