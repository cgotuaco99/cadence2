# Makefile for testing the Cadence

C++ = g++
CFLAGS = -g -std=gnu++11
SUPP = Supp


C2P01MSG = $(info ********COMPILING CADENCE 2 - PART 1***********)
C2P02MSG = $(info ********COMPILING CADENCE 2 - PART 2***********)
C2P03MSG = $(info ********COMPILING CADENCE 2 - PART 3***********)
cleanMsg = $(info ********make clean************)
allMsg = $(info ********COMPILING All Problems************)

C2P01 = C2P01
C2P02 = C2P02
C2P03 = C2P03

C2P01Comp = $(C++) $(CFLAGS) $(C2P01).cpp $(SUPP).cpp -o $(C2P01)
C2P02Comp = $(C++) $(CFLAGS) $(C2P02).cpp $(SUPP).cpp -o $(C2P02)
C2P03Comp = $(C++) $(CFLAGS) $(C2P03).cpp $(SUPP).cpp -o $(C2P03)
cleanAll = $(RM) $(C2P01) $(C2P02) $(C2P03) *.o *.out

P01:
	$(C2P01MSG)
	$(C2P01Comp)
P02:
	$(C2P02MSG)
	$(C2P02Comp)
P03:
	$(C2P03MSG)
	$(C2P03Comp)
all:
	$(allMsg)
	$(C2P01Comp)
	$(C2P02Comp)
clean:
	$(cleanMsg)
	$(cleanAll)
