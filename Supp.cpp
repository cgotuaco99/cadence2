/**********************************************
* File: Supp.cpp
* Author: Chris Gotuaco, Wonseok Lee, Max Nguyen, Matthew Shan
* Email: cgotuaco@nd.edu, wlee11@nd.edu, mnguye18@nd.edu, mshan@nd.edu
*
**********************************************/
#include "Supp.h"
/********************************************
* Function Name  : getFileStream
* Pre-conditions : std::ifstream &inFile, char* fileName
* Post-conditions: none
*
* creates ifstream and opens the stream with the file, and checks if the file is open
********************************************/
void getFileStream(std::ifstream &inFile, char* fileName){
    // Create ifstream and open the stream with the file
    inFile.open(fileName);

    // Check if the file is open.
    if (!inFile.is_open()){
        std::cerr << "File Not Found! Exiting program..." << std::endl;
        exit(-1);
    }
}

/********************************************
* Function Name  : addNodes
* Pre-conditions : std::ifstream& inputStream, Node<int>* node1, Node<int>* node2, Node<int>* node3
* Post-conditions: none
*
* This function passes an ifstream by reference; also passes in the three node pointers
* This fucntion also reads the input from the file to the data values from the data in the struct pointers 
********************************************/
void addNodes(std::ifstream& inputStream, Node<int>* node1, Node<int>* node2, Node<int>* node3){
    inputStream >> node1->data;
    inputStream >> node2->data;
    inputStream >> node3->data;
}

/********************************************
* Function Name  : addNodes
* Pre-conditions : std::ifstream& inputStream, DLList<int>& theList
* Post-conditions: none
* 
* addNodes function is overloaded
********************************************/
void addNodes(std::ifstream& inputStream, DLList<int>& theList){
    int data;
    while(inputStream >> data){
        theList.insert(data);
    }
}

/********************************************
* Function Name  : setPtrs
* Pre-conditions : Node<int>* node, Node<int>* prev, Node<int>* next
* Post-conditions: none
*
* This function takes in three node pointers, and then assigns one to previous and one to next. 
********************************************/
void setPtrs(Node<int>* node, Node<int>* prev, Node<int>* next){
    node->prev = prev;
    node->next = next;
}

/********************************************
* Function Name  : printPtrs
* Pre-conditions : std::ostream& out, Node<int>* node
* Post-conditions: none
*
* This function takes in an ostream by reference and passes a node
* Prints previous and next data to screen
* checks for the null pointer: if null pointer is reached, segmentation fault will occur
********************************************/
void printPtrs(std::ostream& out, Node<int>* node){
    if(node->prev != nullptr){
        out << "Previous: " << node->prev->data << std::endl;
    }
    if(node != nullptr){
        out << "Current: " << node->data << std::endl;
    }
    if(node->next != nullptr){
        out << "Next: " << node->next->data << std::endl;
    }
    out << std::endl;
}
