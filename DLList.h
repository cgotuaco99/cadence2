/**********************************************
* File: DLList.h
* Author: Chris Gotuaco, Wonseok Lee, Max Nguyen, Matthew Shan
* Email: cgotuaco@nd.edu, wlee11@nd.edu, mnguye18@nd.edu, mshan@nd.edu
*
**********************************************/
#ifndef DLLIST_H
#define DLLIST_H
#include <stdexcept>
#include "DLLNode.h"
template<class T>
class DLList{
    public:
        Node<T> *head;
        Node<T> *tail;

        /********************************************
        * Function Name  : DLList<T>
        * Pre-conditions : none
        * Post-conditions: none
        * creates a node with a nullptr; empty constructor
        ********************************************/
        DLList<T>() {
            // new Node() creates a Node will a nullptr
            this->head = new Node<T>();
        };

        /********************************************
        * Function Name  : ~DLList<T>
        * Pre-conditions : none
        * Post-conditions: none
        * deconstructor
		* set current pointer to next pointer, iterates through until nullptr is reached
        ********************************************/
        ~DLList<T>() {
            Node<T>* current = head; // (1)
            while (current != nullptr) {
                Node<T>* next = current->next; // (2)
                delete current; // (3)
                current = next; // (4)
            }
            head = nullptr;
        };

        /********************************************
        * Function Name  : insert
        * Pre-conditions : T value
        * Post-conditions: none
        * inserts value as new node
        ********************************************/
        void insert(T value) {
            if (head->next == NULL) {
                //head = new Node<T>();
                tail = head; // Case 1: (1)
                head->next = tail; // Case 1: (2)
                head->data = value; // Case 1: (3)
            } else {
                tail->next = new Node<T>(); // Case 2: (1)
                tail->next->prev = tail; // Case 2: (2)
                tail = tail->next; // Case 2: (3)
                tail->data = value; // Case 2: (4)
                tail->next = nullptr; // Case 2: (5)
            }
        };

        /********************************************
        * Function Name  : deleteNode
        * Pre-conditions : T key
        * Post-conditions: none
        * deletes node given key
        ********************************************/
        void deleteNode(T key) {
            if(head == nullptr)
                throw std::out_of_range("invalid LinkedList Node");
            else if(head->data == key){ // Case 2
                head = head->next; // Case 2 : (1)
                if(head != nullptr) // Case 2 : (2)
                    head->prev = nullptr;
                return;
            }
            Node<T>* current = head;
            Node<T>* previous = nullptr;
            while(current != nullptr && current->data != key){
                previous = current;
                current = current->next;
            }
            //delete cur node
            previous->next = current->next; // Case 3: (1)
            if(previous->next != nullptr){
                previous->next->prev = previous; // Case 3: (2)
            }
            if(current == tail){
                tail = nullptr;
                previous->next = tail; // Case 3: (3)
                tail = previous; // Case 4: (4)
            }
            delete current;
        }

        /********************************************
        * Function Name  : operator<<
        * Pre-conditions : std::ostream& stream, const DLList<T>& theList
        * Post-conditions: friend std::ostream&
        * overloaded ostream operator
        ********************************************/
        friend std::ostream& operator<< (std::ostream& stream, const DLList<T>& theList){
            Node<T>* temp;
            if (theList.head == NULL) {
                return stream;
            }
            temp = theList.head;
            while (temp != NULL) {
                stream << temp->data << " " ;
                temp = temp->next;
            }
            return stream;
        };
};

#endif
