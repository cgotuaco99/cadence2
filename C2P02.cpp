/**********************************************
* File: C2P02.cpp
* Author: Chris Gotuaco, Wonseok Lee, Max Nguyen, Matthew Shan
* Email: cgotuaco@nd.edu, wlee11@nd.edu, mnguye18@nd.edu, mshan@nd.edu
*  
**********************************************/
#include "Supp.h"
/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
* 
* This function creates three instances of Node pointers.
* Instantiates new instances of the node pointer to have the same template as the pointer
* adds Nodes, sets pointers, then prints out node pointers 
********************************************/
int main(int argc, char **argv){
    std::ifstream inputStream;
    getFileStream(inputStream, argv[1]);
    Node<int>* node1 = new Node<int>();
    Node<int>* node2 = new Node<int>();
    Node<int>* node3 = new Node<int>();
    addNodes(inputStream, node1, node2, node3);
    setPtrs(node1, nullptr, node2);
    setPtrs(node2, node1, node3);
    setPtrs(node3, node2, nullptr);
    printPtrs(std::cout, node1);
    printPtrs(std::cout, node2);
    printPtrs(std::cout, node2);
    return 0;
}
