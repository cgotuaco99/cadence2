/**********************************************
* File: Supp.h
* Author: Chris Gotuaco, Wonseok Lee, Max Nguyen, Matthew Shan
* Email: cgotuaco@nd.edu, wlee11@nd.edu, mnguye18@nd.edu, mshan@nd.edu
*
**********************************************/
#ifndef SUPP_H
#define SUPP_H
#include <iostream>
#include <fstream>
#include "DLList.h"
#include "DLLNode.h"

/********************************************
* Function Name  : getFileStream
* Pre-conditions : std::ifstream &stream, char* fileName
* Post-conditions: none
*
********************************************/
void getFileStream(std::ifstream &stream, char* fileName);

/********************************************
* Function Name  : addNodes
* Pre-conditions : std::ifstream& inputStream, Node<int>* node1, Node<int>* node2, Node<int>* node3
* Post-conditions: none
*
********************************************/
void addNodes(std::ifstream& inputStream, Node<int>* node1, Node<int>* node2, Node<int>* node3);

/********************************************
* Function Name  : addNodes
* Pre-conditions : std::ifstream& inputStream, DLList<int>& theList
* Post-conditions: none
*  
********************************************/
void addNodes(std::ifstream& inputStream, DLList<int>& theList);

/********************************************
* Function Name  : setPtrs
* Pre-conditions : Node<int>* node, Node<int>* prev, Node<int>* next
* Post-conditions: none
*
********************************************/
void setPtrs(Node<int>* node, Node<int>* prev, Node<int>* next);

/********************************************
* Function Name  : printPtrs
* Pre-conditions : std::ostream& out, Node<int>* node
* Post-conditions: none
*
********************************************/
void printPtrs(std::ostream& out, Node<int>* node);

#endif
